'use strict';

function changeTableColor(txtColor){/*
                                    This function change the text color of the table to the input color (txtColor)  
                                    */
    document.getElementsByTagName('table')[0].style.color=txtColor;
}

function changeBackground(bgColor){/*
                                    This function change the background color of the table to the input color (bgColor)  
                                    */
    document.getElementsByTagName('table')[0].style.backgroundColor=bgColor;
} 

function changeTagWidth(width){/*
                                This function change the width of the table in % to the input number (width)  
                                */
    document.getElementsByTagName('table')[0].style.width= width+"%";
}

function changeBorderColor(borderColor){/*
                                        This function change the border color if the table to the input color (borderColor)  
                                        */
    for(let x=0; x<document.getElementsByTagName('td').length; x++ ){
        document.getElementsByTagName('td')[x].style.borderColor=borderColor;
    }
}

function changeBorderWidth(borderThickness){/*
                                            This function change the border thickness of the table in px to the input number (borderThickness)  
                                            */
    for(let x=0; x<document.getElementsByTagName('td').length; x++ ){
        document.getElementsByTagName('td')[x].style.borderWidth= borderThickness+"px";
    }
} 