'use strict';

function start(){/*
                    This function will create all the variables needed for input, then fill these variable with input and  call 
                    the generateTable function to use the input or it will call a related method from utilities.js to change the 
                    color for example. It will then run the function generateTable with all the variables to make sure we have all the variables  
                 */
        let nbRow = document.getElementById('nber-rows');
        let nbCol = document.getElementById('nber-cols');
        let tblWidth = document.getElementById('table-width');
        let tblColor = document.getElementById('text-color');
        let bgColor = document.getElementById('table-background'); 
        let bordWidth = document.getElementById('border-width');
        let bordColor = document.getElementById('border-color');
        nbCol.addEventListener('input', function(){generateTable(nbRow, nbCol, tblWidth, tblColor, bgColor, bordWidth, bordColor);})
        nbRow.addEventListener('input', function(){generateTable(nbRow, nbCol, tblWidth, tblColor, bgColor, bordWidth, bordColor);})
        tblWidth.addEventListener('input', function(){changeTagWidth(tblWidth.value);});
        tblColor.addEventListener('input', function(){changeTableColor(tblColor.value);});
        bgColor.addEventListener('input', function(){changeBackground(bgColor.value);});
        bordWidth.addEventListener('input', function(){changeBorderWidth(bordWidth.value);});
        bordColor.addEventListener('input', function(){changeBorderColor(bordColor.value);})
        generateTable(nbRow, nbCol, tblWidth, tblColor, bgColor, bordWidth, bordColor);
}

function generateTable(nbRow, nbCol, tblWidth, tblColor, bgColor, bordWidth, bordColor){/*
                                                                                        This function will use allthe variables needed from the star function and verify if there is already a table or textarea
                                                                                        to make sure it doesn't duplicate. It will then create a table and textArea that represents the table code. After this it 
                                                                                        changes the attribute of the table to the attribue of the variable. 
                                                                                        */

    //Verify it tbl/txtarea exists, if it is, it will delete the table/textarea 
    if(document.getElementsByTagName('table')[0] != null, document.getElementsByTagName('textarea')[0] != null ){
        document.getElementsByTagName('table')[0].remove();
        document.getElementsByTagName('textarea')[0].remove();
    }
    //Create the table 
    let tbl = document.createElement('table');
    document.getElementById('tbl-render-space').appendChild(tbl);
    //Create textArea
    let txtArea = document.createElement('textarea');
    document.getElementById('tbl-html-space').appendChild(txtArea);
    txtArea.innerHTML = "<table>\n";
    //Filling the table
    for(let x=0; x < nbRow.value; x++){
        txtArea.innerHTML = txtArea.innerHTML + '&emsp;&emsp;<tr>\n';
        //Create row
        let row = document.createElement('tr');
        tbl.appendChild(row);
        for(let y=0; y < nbCol.value; y++){
            txtArea.innerHTML = txtArea.innerHTML + '&emsp;&emsp;&emsp;&emsp;<td> case ('+y+','+x+') </td>\n';
            //Create cell+filling it 
            let cell = document.createElement('td')
            cell.textContent='Cell ('+y+','+x+')';
            row.appendChild(cell);
        }
        txtArea.innerHTML = txtArea.innerHTML + '&emsp;&emsp;</tr>\n';
    }
    txtArea.innerHTML = txtArea.innerHTML+ '</table>';
    tbl.setAttribute("table-width", tblWidth.value);
    tbl.setAttribute("color", tblColor.value);
    tbl.setAttribute("background-color", bgColor.value);
    tbl.setAttribute("border", bordWidth.value+"px");
    tbl.setAttribute("border", bordColor.value);
}

window.addEventListener('DOMContentLoaded', start); /*
                                                     Launch the start function when the page finish loading
                                                    */
